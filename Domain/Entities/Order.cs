﻿namespace Domain.Entities
{
    public class Order:BaseEntity
    {
        public DateTime PurchaseDate { get; set; }
        public decimal OrderTotal { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public int UserId { get; set; }
        public User User;
        public ICollection<OrderDetail> OrderDetails { get; set; }
    }

    public enum OrderStatus
    {
        InProgress,
        Confirmed,
        Shipping,
        Success,
        Canceled
    }
}