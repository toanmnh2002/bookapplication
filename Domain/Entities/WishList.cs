﻿namespace Domain.Entities
{
    public class WishList : BaseEntity
    {
        public int UserId { get; set; }
        public int BookId { get; set; }
        public User User { get; set; }
        public Book Book { get; set; }
    }
}
