﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class CartItem:BaseEntity
    {
        public int BookId { get; set; }
        public int Quantity { get; set; }
        [NotMapped]
        public decimal Price { get; set; }
        [NotMapped]
        public string Title{ get; set; }
        public Book Book { get; set; }
        public int CartId { get; set; }
        public Cart Cart { get; set; }

    }
}
