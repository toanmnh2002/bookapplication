﻿
namespace Domain.Entities
{
    public class User : BaseEntity
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public decimal CreditBalance { get; set; }
        public decimal Points { get; set; }
        public ICollection<Order> OrderHistory { get; set; }
        public ICollection<Book> WishedBooks { get; set; }
        public ICollection<BookReview> BookReviews { get; set; }
        public Cart Cart { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
