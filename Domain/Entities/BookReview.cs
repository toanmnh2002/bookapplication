﻿namespace Domain.Entities
{
    public class BookReview : BaseEntity
    {
        public int BookId { get; set; }
        public int UserId { get; set; }
        public decimal Rating { get; set; }
        public Book Book { get; set; }
        public User User { get; set; }
    }
}
