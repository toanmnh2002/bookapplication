﻿namespace Domain.Entities
{
    public class Author : BaseEntity
    {
        public int Name { get; set; }
        public ICollection<Book> Books { get; set; }
    }
}
