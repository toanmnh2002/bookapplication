﻿namespace Domain.Entities
{
    public class Book:BaseEntity
    {
        //One book can have multiple reviews.
        public string Title { get; set; }
        public string CoverImage { get; set; }
        public decimal Price { get; set; }
        public string NamePlural { get; set; }
        public ICollection<Author> Authors { get; set; }
        public ICollection<User> WishedBy { get; set; }
        public ICollection<BookAuthor> BookAuthor { get; set; }
        public ICollection<CartItem> CartItems { get; set; }
        public ICollection<OrderDetail> OrderDetails { get; set; }
        public ICollection<BookReview> BookReviews { get; set; }
    }
}
