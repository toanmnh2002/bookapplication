//using Application.SeedData;
//using Domain.Entities;
//using Microsoft.EntityFrameworkCore;
//using Microsoft.Extensions.DependencyInjection;
//using Microsoft.AspNetCore.Builder;


//namespace Infrastructures.SeedData
//{
//    public static class SeedDataForTesting
//    {
//        public static async Task SeedDataInit(WebApplication app)
//        {
//            using var serviceScope = app.Services.CreateScope();
//            using var context = serviceScope.ServiceProvider.GetService<AppDBContext>()!;

//            await SeedDataAsync<Author>(context);
//            await SeedDataAsync<Book>(context);
//            await SeedDataAsync<BookAuthor>(context);
//            await SeedDataAsync<BookReview>(context);
//            await SeedDataAsync<Cart>(context);
//            await SeedDataAsync<CartItem>(context);
//            await SeedDataAsync<Order>(context);
//            await SeedDataAsync<OrderDetail>(context);
//            await SeedDataAsync<Payment>(context);
//            await SeedDataAsync<User>(context);
//            await SeedDataAsync<WishList>(context);
//            await SeedDataAsync<User>(context);

//            await context.SaveChangesAsync();
//        }

//        private static async Task SeedDataAsync<TEntity>(AppDBContext context) where TEntity : class
//        {
//            var dbSet = context.Set<TEntity>();
//            var dataExists = await dbSet.AnyAsync();
//            if (!dataExists)
//            {
//                var data = await DataInitializer.SeedDataAsync<TEntity>();

//                dbSet.AddRange(data);
//            }
//        }

//        //private static async Task SeedAttendancesDataAsync(AppDBContext context)
//        //{
//        //    var dataExists = await context.Attendances.AnyAsync();
//        //    if (!dataExists)
//        //    {
//        //        var attendances = await DataInitializer.SeedDataAsync<Attendance>();

//        //        context.Attendances.AddRange(attendances);
//        //    }
//        //}
//    }
//}
