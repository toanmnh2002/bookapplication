﻿using Application;
using Application.IRepositories;
using Infrastructures.Repositories;

namespace Infrastructures
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDBContext _dbContext;
        private readonly IUserRepository _userRepository;
        private readonly ICartRepository _cartRepository;
        private readonly IBookRepository _bookRepository;

        public UnitOfWork(AppDBContext dbContext)
        {
            _dbContext = dbContext;
            _userRepository = new UserRepository(_dbContext);
            _cartRepository = new CartRepository(_dbContext);
            _bookRepository = new BookRepository(_dbContext);
        }


        public IUserRepository UserRepository => _userRepository;
        public ICartRepository CartRepository => _cartRepository;
        public IBookRepository BookRepository => _bookRepository;

        public AppDBContext AppDBContext => _dbContext;

        public async Task<int> SaveChangeAsync() => await _dbContext.SaveChangesAsync();
    }
}
