﻿using Application.CartDTO;
using AutoMapper;
using Domain.Entities;


namespace Infrastructures.Mappers
{
    public class MapperConfiguration : Profile
    {
        public MapperConfiguration()
        {
            CreateMap<Cart,CartResponseDTO>().ReverseMap();
            CreateMap<CartDTO,CartItem>().ForMember(dest => dest.BookId, opt => opt.MapFrom(src => src.BookId)).ReverseMap();
            CreateMap<CartItem,CartItemResponseDTO>().ForMember(dest=>dest.Id,opt=>opt.MapFrom(src=>src.BookId)).ReverseMap();
        }
    }
}
