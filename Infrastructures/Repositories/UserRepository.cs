﻿using Application.IRepositories;
using Domain.Entities;


namespace Infrastructures.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(AppDBContext appDBContext) : base(appDBContext)
        {
        }
    }
}
