﻿using Application.IRepositories;
using Domain.Entities;


namespace Infrastructures.Repositories
{
    public class CartRepository : GenericRepository<Cart>, ICartRepository
    {
        public CartRepository(AppDBContext appDBContext) : base(appDBContext)
        {
        }
    }
}
