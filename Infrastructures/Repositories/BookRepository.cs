﻿using Application.IRepositories;
using Domain.Entities;


namespace Infrastructures.Repositories
{
    public class BookRepository : GenericRepository<Book>, IBookRepository
    {
        public BookRepository(AppDBContext appDBContext) : base(appDBContext)
        {
        }
    }
}
