﻿
using Application.IRepositories;
using Domain.Entities;
using Global.Services.Commons;
using Global.Services.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using System.Linq.Expressions;

namespace Infrastructures.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        protected readonly DbSet<T> _dbSet;
        private readonly AppDBContext _appDBContext;

        public GenericRepository(AppDBContext appDBContext)
        {
            this._appDBContext = appDBContext;
            _dbSet = appDBContext.Set<T>();
        }
        public async Task AddEntityAsync(T obj)
        {
            await _dbSet.AddAsync(obj);
        }
        public async Task<T> AddEntityAsyncGeneric(T obj)
        {
            await _dbSet.AddAsync(obj);
            return obj;
        }
        public async Task AddEntityRange(ICollection<T> objs)
        {
            await _dbSet.AddRangeAsync(objs);
        }
        public void UpdateEntity(T obj)
        {
            _dbSet.Update(obj);
        }
        public void UpdateRange(ICollection<T> objs)
        {
            _dbSet.UpdateRange(objs);
        }
        public void SoftRemoveEntity(T obj)
        {
            _dbSet.Remove(obj);
        }
        public void SoftRemoveEntityById(T obj)
        {
            _dbSet.Remove(obj);
        }
        public void SoftRemoveEntityRange(ICollection<T> objs)
        {
            _dbSet.RemoveRange(objs);
        }
        public async Task<IEnumerable<T>> ExecuteQueryAsync(string sqlQuery)
        {
            return await _dbSet.FromSqlRaw(sqlQuery).ToListAsync();
        }
        public async Task<IEnumerable<T>> GetListAsync()
        {
            var entities = await _dbSet.AsQueryable().ToListAsync();
            return entities;
        }
        public async Task<IEnumerable<T>> GetListAsync(bool isTracked = true)
        {
            if (isTracked)
            {
                List<T> cachedDatas = await _dbSet.ToListAsync();
                return cachedDatas;
            }
            else
            {
                List<T> cachesDatas = await _dbSet.AsNoTracking().ToListAsync();
                return cachesDatas;
            }
        }
        public async Task<IEnumerable<T>> GetListAsync(params string[] otherEntities)
        {
            IQueryable<T> entities = null;
            foreach (string other in otherEntities)
            {
                if (entities == null)
                {
                    entities = _dbSet.Include(other);
                }
                else
                {
                    entities = entities.Include(other);
                }
            }
            return await entities.ToListAsync();
        }
        public List<T> GetAllByCondition(Expression<Func<T, bool>> predicate)
        {
            return _dbSet.Where(predicate).ToList();
        }
        public async Task<IEnumerable<T>>
            GetAllMultiIncludeAsync(
                Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null,
                bool disableTracking = true)
        {
            IQueryable<T> query = _dbSet;
            if (disableTracking)
            {
                query = query.AsNoTracking();
            }
            query = include(query);
            return await query.ToListAsync();
        }
        public async Task<T> GetEntityByIdAsync(Guid id)
        {
            return await _dbSet.FindAsync(id);
        }
        public Task<Pagination<T>> FindAsync(
            Expression<Func<T, bool>>? predicate = null,
            SortingConditionQueue<T>? sortConditions = null,
            int pageIndex = 0, int pageSize = 10)
        {
            return _dbSet
                    .WhereIfNotNull(predicate)
                    .OrderBy(sortConditions)
                    .PaginateAsync(pageIndex, pageSize);
        }
        public Task<Pagination<T>> ToPagination(int pageIndex = 0, int pageSize = 10)
        {
            return _dbSet.PaginateAsync(pageIndex, pageSize);
        }

        public Task<T?> GetEntityByIdAsync(int id)
        {
            throw new NotImplementedException();
        }

        Task<List<T>> IGenericRepository<T>.GetListAsync()
        {
            throw new NotImplementedException();
        }
    }
    
}
