﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.FluentAPIs
{
    public class BookReviewConfiguration
    {
        public class OrderDetailConfiguration : IEntityTypeConfiguration<BookReview>
        {
            public void Configure(EntityTypeBuilder<BookReview> builder)
            {
                builder.Ignore(x => x.Id);
                builder.HasKey(x => new { x.BookId, x.UserId });
                builder.HasOne(x => x.Book)
                    .WithMany(x => x.BookReviews)
                    .HasForeignKey(x => x.BookId);
                builder.HasOne(x => x.User)
                    .WithMany(x => x.BookReviews)
                    .HasForeignKey(x => x.UserId);
            }
        }
    }
}
