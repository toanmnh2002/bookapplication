﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.HasKey(x => x.Id);
            //builder.HasQueryFilter(x => !x.IsDeleted);
            builder.HasOne(x => x.User)
                .WithMany(x => x.OrderHistory)
                .HasForeignKey(x => x.UserId);
        }
    }
}
