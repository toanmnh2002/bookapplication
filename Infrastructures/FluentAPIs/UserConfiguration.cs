﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasMany(x => x.WishedBooks)
                .WithMany(x => x.WishedBy)
                .UsingEntity<WishList>(
                x => x.HasOne(x => x.Book).WithMany().HasForeignKey(x => x.BookId),
                x => x.HasOne(x => x.User).WithMany().HasForeignKey(x => x.UserId)
                );
            builder.Property(x => x.RowVersion)
                .IsRowVersion();

        }
    }
}
