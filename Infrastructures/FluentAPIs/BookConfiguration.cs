﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class BookConfiguration : IEntityTypeConfiguration<Book>
    {
        public void Configure(EntityTypeBuilder<Book> builder)
        {
            builder.HasKey(a => a.Id);
            builder.HasMany(x => x.Authors)
                    .WithMany(x => x.Books)
                    .UsingEntity<BookAuthor>(
                         x => x.HasOne(x => x.Author).WithMany(),
                         x => x.HasOne(x => x.Book).WithMany(x => x.BookAuthor)
                    );
        }
    }
}
