﻿using System.Diagnostics;
using System.Text;
using Application.Commons;
using Application.ViewModels.UserViewModel;
using BookApplication.InterfaceServices;
using BookApplication.Middlewares;
using BookApplication.Services;
using FluentValidation.AspNetCore;
using FluentValidation;
using Global.Services.JsonConverters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using BookApplication.FluentValidation;

namespace BookApplication
{
    public static class DependencyInjection
    {
        public static IServiceCollection APIServices(this IServiceCollection services, AppConfiguration configuration)
        {
            {
                services.AddHttpContextAccessor();
                services.AddScoped<IClaimService, ClaimService>();
            }
            {
                services.AddControllers()
                .ConfigureApiBehaviorOptions(options => { options.SuppressModelStateInvalidFilter = true; })
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.Converters.Add(new NullableDateOnlyJsonConverter());
                    options.JsonSerializerOptions.Converters.Add(new DateOnlyJsonConverter());
                });
                services.AddSwaggerGen();
                services.AddEndpointsApiExplorer();
            }
            {
                services.AddFluentValidationAutoValidation();
                services.AddFluentValidationClientsideAdapters();
                services.AddScoped<IValidator<LoginUserViewModel>, LoginValidation>();
                services.AddScoped<IValidator<RegisterViewModel>, RegisterValidation>();
                services.AddScoped<IValidator<UpdateUserViewModel>, UpdateUserValidation>();

                services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                }).AddJwtBearer(o =>
                {
                    o.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        IssuerSigningKey = new SymmetricSecurityKey
                        (Encoding.UTF8.GetBytes(configuration.JWTSecretKey))
                    };
                });
            }
            services.AddHealthChecks();
            services.AddSingleton<GlobalExceptionMiddleware>();
            services.AddSingleton<PerformanceMiddleware>();
            services.AddSingleton<Stopwatch>();

            services.AddHttpClient();
            return services;
        }
    }
}
