﻿using Application.ViewModels.UserViewModel;
using FluentValidation;
using System.Data;

namespace BookApplication.FluentValidation
{
    public class RegisterValidation : AbstractValidator<RegisterViewModel>
    {
        public RegisterValidation()
        {
            RuleFor(x => x.Username).NotEmpty()
                                               .NotNull()
                                               .WithMessage("User Name cannot be Null or Empty!!!");
            RuleFor(x => x.Password).NotEmpty()
                                    .NotNull()
                                    .WithMessage("Password cannot be Null or Empty!!!");
            RuleFor(x => x.Email).NotEmpty()
                                 .NotNull()
                                 .EmailAddress()
                                 .WithMessage("Invalid Email!");
        }
    }
}
