﻿using Application.ViewModels.UserViewModel;
using FluentValidation;

namespace BookApplication.FluentValidation
{
    public class LoginValidation : AbstractValidator<LoginUserViewModel>
    {
        public LoginValidation()
        {
            RuleFor(x => x.Username).NotEmpty()
                                    .NotNull()
                                    .WithMessage("User Name cannot be Null or Empty!!!");
            RuleFor(x => x.Password).NotEmpty()
                                    .NotNull()
                                    .WithMessage("Password cannot be Null or Empty!!!");
        }
    }
}
