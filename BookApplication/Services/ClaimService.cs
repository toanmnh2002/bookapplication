﻿using BookApplication.InterfaceServices;
using System.Security.Claims;

namespace BookApplication.Services
{
    public class ClaimService : IClaimService
    {
        public ClaimService(IHttpContextAccessor httpContextAccessor)
        {
            var Id = httpContextAccessor.HttpContext?.User?.FindFirstValue("userID");
            GetCurrentUserId = string.IsNullOrEmpty(Id) ? 0 : int.Parse(Id);
        }
        public int GetCurrentUserId { get; }
    }
}
