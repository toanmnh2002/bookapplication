﻿using Application.IService;
using Application.ViewModels.UserViewModel;
using AutoMapper;
using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BookApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : BaseController
    {
        private readonly IUserService _userService;
        private readonly IValidator<LoginUserViewModel> _loginUserViewModel;
        private readonly IValidator<RegisterViewModel> _registerViewModel;
        private readonly IValidator<UpdateUserViewModel> _updateViewModel;


        public UserController(IUserService userService, IValidator<LoginUserViewModel> loginUserViewModel, IValidator<RegisterViewModel> registerViewModel, IValidator<UpdateUserViewModel> updateViewModel)
        {
            _userService = userService;
            _loginUserViewModel = loginUserViewModel;
            _registerViewModel = registerViewModel;
            _updateViewModel = updateViewModel;
        }

        // GET: api/<UserController>
        [HttpGet("GetAllUser")]
        [AllowAnonymous]
        public async Task<IEnumerable<UserViewModel>> GetAllUser() => await _userService.GetUsersAsync();

        // POST api/<UserController>
        [HttpPost("Register")]
        [AllowAnonymous]
        public async Task<IActionResult> Register(RegisterViewModel registerUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var validation = await _registerViewModel.ValidateAsync(registerUser);
            if (validation.IsValid)
            {
                var result = await _userService.RegisterAsync(registerUser);
                if (result is not null)
                {
                    return Ok(result);
                }
            }
            return BadRequest("Register Fail!");
        }
        [HttpPost("Login")]
        [AllowAnonymous]
        //toan_123
        public async Task<IActionResult> Login(LoginUserViewModel loginUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var validation = await _loginUserViewModel.ValidateAsync(loginUser);
            if (validation.IsValid)
            {
                var result = await _userService.LoginAsync(loginUser);
                if (result is not null)
                {
                    return Ok(result);
                }
            }
            return BadRequest("Login Fail!");
        }
    }
}
