﻿using Application.CartDTO;
using Application.IService;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BookApplication.Controllers
{
    public class CartController : BaseController
    {
        private readonly ICartService _cartService;
        private readonly IMapper _mapper;

        public CartController(ICartService cartService, IMapper mapper)
        {
            _cartService = cartService;
            _mapper = mapper;
        }
        [HttpPost]
        public async Task<IActionResult> CreateNewCartAsync([FromBody] CartContentDTO cartContentDTO)
        {
            var cart = await _cartService.CreateShoppingCartAsync(cartContentDTO);
            var result = _mapper.Map<CartResponseDTO>(cart);
            return CreatedAtRoute("GetCartByIdAsync", new { cartId = cart.Id }, result);
        }
        [HttpPost("{cartId}")]
        public async Task<IActionResult> UpdateCartAsync(Guid cartId, [FromBody] CartResponseDTO cartResponseDTO)
        {
            var cart = await _cartService.UpdateShoppingCartAsync(cartId, cartResponseDTO);
            if (cart == null)
                return NotFound();

            var result = _mapper.Map<CartResponseDTO>(cart);
            return Ok(result);
        }
        [HttpGet("{carId}", Name = "GetCartByIdAsync")]
        public async Task<IActionResult> GetCartByIdAsync(Guid cartId)
        {
            var cart = await _cartService.FindByIdAsync(cartId);
            if (cart == null)
                return NotFound();

            var result = _mapper.Map<CartResponseDTO>(cart);
            return Ok(result);
        }
        [HttpDelete("{cartId}")]
        public async Task<IActionResult> RemoveCartByIdAsync(Guid cartId)
        {
            var cart = await _cartService.FindByIdAsync(cartId);
            if (cart == null)
                return NotFound();

            await _cartService.RemoveShoppingCartAsync(cartId);
            return NoContent();
        }
        [HttpDelete("{cartId}/{bookId}")]
        public async Task<IActionResult> RemoveShoppingCartItemAsync(Guid cartId,
            [FromRoute] Guid bookId)
        {
            var cart = await _cartService.RemoveShoppingCartItemAsync(cartId, bookId);
            if (cart == null)
                return NotFound();

            var result = _mapper.Map<CartResponseDTO>(cart);
            return Ok(result);
        }
        [HttpPost("{cartId}/{bookId}/increase/{quantity}")]
        public async Task<IActionResult> IncreaseShoppingCartItemAsync(Guid cartId,
            [FromRoute] Guid bookId,
            [FromRoute] int quantity = 1)
        {
            var cart = await _cartService.IncreaseShoppingCartItemAsync(cartId, bookId, quantity);

            if (cart == null)
                return NotFound();

            var result = _mapper.Map<CartResponseDTO>(cart);
            return Ok(result);
        }
        [HttpPost("{cartId}/{bookId}/decrease/{quantity}")]
        public async Task<IActionResult> DecreaseShoppingCartItemAsync(Guid cartId,
            [FromRoute] Guid bookId,
            [FromRoute] int quantity = 1)
        {
            var cart = await _cartService.DecreaseShoppingCartItemAsync(cartId, bookId, quantity);

            if (cart == null)
                return NotFound();
            var result = _mapper.Map<CartResponseDTO>(cart);
            return Ok(result);
        }
    }
}
