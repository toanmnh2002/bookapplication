﻿namespace Global.Services.Commons
{
    public enum SortingDirection
    {
        Ascending, Descending
    }
}
