﻿namespace Global.Services.Helpers
{
    public enum PasswordVerificationResult : byte
    {
        Failed,
        Success,
        SuccessRehashNeeded
    }
}
