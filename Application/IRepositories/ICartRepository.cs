﻿using Domain.Entities;

namespace Application.IRepositories
{
    public interface ICartRepository : IGenericRepository<Cart>
    {
    }
}
