﻿using Domain.Entities;

namespace Application.IRepositories
{
    public interface IBookRepository:IGenericRepository<Book>
    {
    }
}
