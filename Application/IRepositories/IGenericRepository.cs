﻿using Domain.Entities;



namespace Application.IRepositories
{
    public interface IGenericRepository<T> where T : BaseEntity
    {
        Task AddEntityAsync(T obj);
        Task AddEntityRange(ICollection<T> objs);
        void UpdateEntity(T obj);
        void UpdateRange(ICollection<T> objs);
        void SoftRemoveEntity(T obj);
        void SoftRemoveEntityRange(ICollection<T> objs);
        Task<T?> GetEntityByIdAsync(int id);
        Task<List<T>> GetListAsync();
    }
}
