﻿

using Application.CartDTO;
using Domain.Entities;

namespace Application.IService
{
    public interface ICartService
    {
        Task<Cart> CreateShoppingCartAsync(CartContentDTO addBookToCart);
        Task<Cart> UpdateShoppingCartAsync(int cartId, CartResponseDTO cartResponseDTO);
        Task RemoveShoppingCartAsync(int cartId);
        Task<Cart> RemoveShoppingCartItemAsync(int cartId, int itemId);
        Task<Cart> IncreaseShoppingCartItemAsync(int cartId, int itemId, int quantity);
        Task<Cart> DecreaseShoppingCartItemAsync(int cartId, int itemId, int quantity);
        Task<Cart> FindByIdAsync(int id);
    }
}
