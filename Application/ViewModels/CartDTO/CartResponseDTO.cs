﻿namespace Application.CartDTO
{
    public class CartResponseDTO
    {
        public int Id { get; set; }
        public decimal Total => Books.Sum(x => x.SubTotal);
        public List<CartItemResponseDTO> Books { get; set; } = new List<CartItemResponseDTO>();
    }
}
