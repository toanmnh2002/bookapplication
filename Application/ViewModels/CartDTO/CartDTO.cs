﻿namespace Application.CartDTO
{
    public class CartDTO
    {
        public int CartId { get; set; }
        public int BookId { get; set; }
        public int Quantity { get; set; }
    }
}
