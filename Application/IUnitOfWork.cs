﻿using Application.IRepositories;

namespace Application
{
    public interface IUnitOfWork
    {
        public IBookRepository BookRepository { get; }
        public ICartRepository CartRepository { get; }
        public IUserRepository UserRepository { get; }
        public Task<int> SaveChangeAsync();
    }
}
