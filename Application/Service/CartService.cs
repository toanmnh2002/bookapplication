﻿using Application;
using Application.CartDTO;
using Application.IService;
using AutoMapper;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace BookApplication.Service
{
    public class CartService : ICartService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CartService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        private async Task<Cart> EnrichCart(Cart cart)
        {
            if (cart is null)
                return null;

            foreach (var item in cart.CartItems)
            {
                var book = await _unitOfWork.BookRepository.GetEntityByIdAsync(item.BookId);
                item.Price = book.Price;
                item.Title = item.Quantity > 1 ? book.NamePlural : book.Title;
            }

            return cart;
        }
        public async Task<Cart> CreateShoppingCartAsync(CartContentDTO cartContentDTO)
        {
            var cart = new Cart();
            if (cartContentDTO.Books.Any())
            {
                cart.CartItems = _mapper.Map<List<CartItem>>(cartContentDTO.Books).ToList();
            }
            await _unitOfWork.CartRepository.AddEntityAsync(cart);
            return await EnrichCart(cart);
        }
        public async Task RemoveShoppingCartAsync(int cartId)
        {
            var cart = await _unitOfWork.CartRepository.GetEntityByIdAsync(cartId);
            if (cart != null)
            {
                _unitOfWork.CartRepository.SoftRemoveEntity(cart);
                await _unitOfWork.SaveChangeAsync();
            }
        }
        public async Task<Cart> RemoveShoppingCartItemAsync(int cartId, int itemId)
        {
            var cart = await _unitOfWork.CartRepository.GetEntityByIdAsync(cartId);
            if (cart == null)
                return null;

            var catalogItem = await _unitOfWork.BookRepository.GetEntityByIdAsync(itemId);
            if (catalogItem == null)
                return null;

            var itemToRemove = cart.CartItems.FirstOrDefault(x => x.BookId == itemId);
            if (itemToRemove != null)
            {
                cart.CartItems.Remove(itemToRemove);
                await _unitOfWork.SaveChangeAsync();
            }
            return await EnrichCart(cart);
        }
        public async Task<Cart> UpdateShoppingCartAsync(int cartId, CartResponseDTO cartResponseDTO)
        {
            var cart = await _unitOfWork.CartRepository.GetEntityByIdAsync(cartId);
            if (cart == null)
                return null;

            cart.CartItems = _mapper.Map<List<CartItem>>(cartResponseDTO.Books).ToList();
            cart.UpdatedAt = DateTimeOffset.UtcNow;
            _unitOfWork.CartRepository.UpdateEntity(cart);
            await _unitOfWork.SaveChangeAsync();
            return await EnrichCart(cart);
        }

        public async Task<Cart> IncreaseShoppingCartItemAsync(int cartId, int itemId, int quantity)
        {
            var cart = await _unitOfWork.CartRepository.GetEntityByIdAsync(cartId);
            if (cart == null)
                return null;

            var catalogItem = await _unitOfWork.BookRepository.GetEntityByIdAsync(itemId);
            if (catalogItem == null)
                return null;

            if (cart.CartItems.Any(x => x.CartId == itemId))
            {
                var item = cart.CartItems.First(x => x.BookId == itemId);
                item.Quantity += quantity;
            }
            else
            {
                cart.CartItems.Add(new CartItem
                {
                    CartId = cartId,
                    BookId = itemId,
                    Quantity = quantity
                });
            }
            //_unitOfWork.AppDBContext.Attach(cart);
            await _unitOfWork.SaveChangeAsync();
            return await EnrichCart(cart);
        }
        public async Task<Cart?> FindByIdAsync(int id) => await _unitOfWork.CartRepository.GetEntityByIdAsync(id);
        public async Task<Cart> DecreaseShoppingCartItemAsync(int cartId, int itemId, int quantity)
        {
            var cart = await _unitOfWork.CartRepository.GetEntityByIdAsync(cartId);
            if (cart == null)
                return null;

            var catalogItem = await _unitOfWork.BookRepository.GetEntityByIdAsync(itemId);
            if (catalogItem == null)
                return null;

            // Do not allow the quantity to go below zero
            // Arguably we should delete it but then makes it more difficult to increase from client
            if (cart.CartItems.Any(x => x.BookId == itemId))
            {
                var item = cart.CartItems.First(x => x.BookId == itemId);
                item.Quantity -= (quantity > item.Quantity) ? item.Quantity : quantity;
            }

            // Do not throw error if the item not in the cart
            // Could equally throw a bad request but little benefit
            //_unitOfWork.AppDBContext.Attach(cart);
            //await _unitOfWork.AppDBContext.SaveChangesAsync();
            return await EnrichCart(cart);
        }
    }
}
